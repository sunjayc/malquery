from urllib import request
from urllib.parse import quote as urlquote
from bs4 import BeautifulSoup
import re

from functools import wraps

def int0(str):
	try:
		return int(str)
	except ValueError:
		return 0

def populates(cls):
	def init_wrapper(__init__):
		@wraps(__init__)
		def wrapper(self, *args, **kwargs):
			self._populated = False
			return __init__(self, *args, **kwargs)
		return wrapper
	def populate_wrapper(populate):
		@wraps(populate)
		def wrapper(self, *args, **kwargs):
			if self._populated:
				return
			populate(self, *args, **kwargs)
			self._populated = True
		return wrapper
	cls.__init__ = init_wrapper(cls.__init__)
	cls.populate = populate_wrapper(cls.populate)
	return cls

def remembers(cls):
	@classmethod
	def make(cls, context, id, *args, **kwargs):
		key = cls.__name__ + str(id)
		if key not in context:
			context[key] = cls(context, id, *args, **kwargs)
		return context[key]
	cls.make = make

	return cls


@populates
class Anime:
	TYPE = [
		'???',
		'TV',
		'OVA',
		'Movie',
		'Special',
		'ONA',
		'Music',
	]
	STATUS = [
		'???',
		'Currently Airing',
		'Finished Airing',
		'Not Yet Aired',
	]
	WATCHSTATUS = [
		'???',
		'Watching',
		'Completed',
		'On-Hold',
		'Dropped',
		'???',
		'Plan to Watch',
	]
	def to_(mapping):
		def f(code):
			code = int0(code)
			return mapping[code]
		return f

	MAPPING = {
		'id': (int0, 'series_animedb_id'),
		'title': (str, 'series_title'),
		'type': (to_(TYPE), 'series_type'),
		'status': (to_(STATUS), 'series_status'),
		'episodes': (int0, 'series_episodes'),
		'image_url': (str, 'series_image'),
		#'watched_episodes': (int, 'my_watched_episodes'),
		#'score': (int, 'my_score'),
		#'watched_status': (to_(WATCHSTATUS), 'my_status'),
	}

	anime = {}

	def __init__(self, id, xml=None):
		self.id = id
		self.xml = xml
		self.extradata = {}

		if xml:
			self.populate()

	def __getattr__(self, name):
		if name in self.MAPPING:
			convert = self.MAPPING[name][0]
			return convert('')
		else:
			raise AttributeError

	def populate(self):
		for k, v in self.MAPPING.items():
			convert = v[0]
			setattr(self, k, convert(getattr(self.xml, v[1]).text))

	def attachdata(self, obj, data):
		self.extradata[obj] = data

	def getdata(self, obj):
		return self.extradata[obj]

	def __iter__(self):
		return iter(self.extradata.items())

	def __contains__(self, item):
		return item in self.extradata

	# Keep this in sync with @remembers.make()!
	@classmethod
	def make(cls, context, id, xml=None, **kwargs):
		key = cls.__name__ + str(id)
		if key in context:
			a = context[key]
			if xml and not a.xml:
				a.xml = xml
				a.populate()
			return a
		anime = cls(id, xml)
		for arg in kwargs:
			if arg not in anime.__dict__:
				setattr(anime, arg, kwargs[arg])
		context[key] = anime
		return anime

	@classmethod
	def fromXML(cls, context, xml):
		m_id = cls.MAPPING['id']
		id = m_id[0](getattr(xml, m_id[1]).text)
		return Anime.make(context, id=id, xml=xml)

	def __eq__(self, other):
		return self.id == other.id

	def __hash__(self):
		return hash(self.id)

@populates
class AnimeList:
	def __init__(self, context, name):
		self.name = name
		self.context = context
	
	def populate(self):
		try:
			with open(self.name, 'r') as f:
				self.xml = BeautifulSoup(f, from_encoding='utf-8')
		except IOError:
			url = 'http://myanimelist.net/malappinfo.php?u=%s&status=all&type=anime' \
				% urlquote(self.name)
			with request.urlopen(url) as url:
				self.xml = BeautifulSoup(url.read(), from_encoding='utf-8')
		animes = self.xml.find_all('anime')
		self.anime = []
		for xml in animes:
			self.anime.append(Anime.fromXML(self.context, xml))

class AnimeSet:
	def __init__(self, origin, setdata=None):
		if hasattr(origin, 'anime'):
			self.origin = [origin]
			self._anime = set(origin.anime)
		else:
			self.origin = []
			for orig in origin:
				self.origin.extend(orig)
			self._anime = setdata

	def __iter__(self):
		return iter(self._anime)

	def intersect(self, other):
		return AnimeSet((self.origin, other.origin), self._anime & other._anime)

@populates
@remembers
class Seiyuu:
	def __init__(self, context, id):
		self.id = id
		self.context = context
	
	def populate(self):
		url = 'http://myanimelist.net/people/%d' % self.id
		with request.urlopen(url) as url:
			self.xml = BeautifulSoup(url.read(), from_encoding='utf-8')

		self.name = self.xml.select('#contentWrapper > h1')[0].text.strip()

		maintds = self.xml.select('#content > table > tr')[0].find_all('td')
		self.image_url = maintds[0].div.img['src']
		self.trs = maintds[1].table.find_all('tr')

		self.anime = set()
		for tr in self.trs:
			a = tr('td')[1].a
			id = int(re.search('anime\/(\d+)\/', a['href']).group(1))
			title = a.text.strip()
			image_url = tr.td.div.a.img['src']
			charblock = tr('td')[2]
			char = charblock.a.text.strip()
			type = charblock.div.text.strip()
			anime = Anime.make(self.context, id, title=title, image_url=image_url)
			if self in anime:
				data = anime.getdata(self)
			else:
				data = []
			data.append({'char': char, 'type': type})
			anime.attachdata(self, data)
			self.anime.add(anime)

	def __str__(self):
		return '(%d) %s' % (self.id, self.name)

	def __eq__(self, other):
		return self.id == other.id

	def __hash__(self):
		return hash(self.id)

def _test():
	animelist = AnimeList('shijie.xml')
	print('Populating...')
	animelist.populate()
	anime = animelist.anime[106]
	x = 5
	from code import interact; interact(local=locals())

if __name__ == '__main__':
	_test()
