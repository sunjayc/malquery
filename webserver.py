#!/usr/bin/env python3

from http.server import HTTPServer, BaseHTTPRequestHandler
import urllib.parse
import posixpath
import sys

from xml.etree import ElementTree as etree
from maldata import *

class Context(dict):
	pass

class MalQueryHTTPRequestHandler(BaseHTTPRequestHandler):
	server_version = "MalQueryHTTP/0.1"

	def do_GET(self):
		context = Context()
		context.view = self.view_table

		words = self.parsepath(self.path)
		words.reverse()

		data = []
		while words:
			fn = 'fn_' + words.pop()
			if not hasattr(self, fn):
				self.send_error(404)
				return
			method = getattr(self, fn)
			method(context, words, data)
		with open('myanimelist.v44.mod.css', 'r') as cssfile:
			css = cssfile.read()
		output = etree.XML('<body><style type="text/css">' + css + '</style></body>')
		if data:
			output.append(context.view(data.pop()))
		output = etree.tostring(output, encoding='utf-8')

		self.send_response(200)
		self.send_header("Content-type", "text/html; charset=utf-8")
		self.send_header("Content-Length", str(len(output)))
		self.end_headers()
		self.wfile.write(output)

	def parsepath(self, path):
		path = path.split('?',1)[0]
		path = path.split('#',1)[0]
		path = posixpath.normpath(urllib.parse.unquote(path))
		words = path.split('/')
		words = filter(None, words)
		return [w.lower() for w in words]

	@staticmethod
	def view_text(animeset):
		output = []
		for origin in animeset.origin:
			output.append(str(origin))
		for a in sorted(animeset, key=lambda a: a.title):
			line = a.title + ' | '
			info = []
			for obj, data in a:
				entries = []
				for datum in data:
					entry = []
					for k in datum:
						field = k + ': ' + str(datum[k])
						entry.append(field)
					entry = '; '.join(entry)
					entries.append(entry)
				entries = str(obj) + ' [' + ' # '.join(entries) + ']'
				info.append(entries)
			line += ' / '.join(info)
			output.append(line)
		output = '\n'.join(output)
		pre = etree.Element('pre')
		pre.text = output
		return pre

	@staticmethod
	def view_table(animeset):
		def makeheader():
			headertr = etree.Element('tr')
			headertr.append(etree.Element('td'))
			headertr.append(etree.Element('td'))
			for origin in animeset.origin:
				if isinstance(origin, Seiyuu):
					headertr.append(etree.XML("""
					<td class="borderClass" valign="top" align="right">
						{name}
					</td>
					""".format(name=origin.name)))
			return headertr

		def makeentry(anime):
			from xml.sax.saxutils import escape as xmlescape
			animetr = etree.XML("""
			<tr>
				<td valign="top" class="borderClass" align="center">
					<div class="picSurround" width="68px" height="68px">
						<a href="http://myanimelist.net/anime/{id}">
							<img src="{image_url}" width="auto" height="auto" vspace="4" hspace="8" border="0" />
						</a>
					</div>
				</td>
				<td valign="top" class="borderClass">
					<a href="http://myanimelist.net/anime/{id}">{title}</a>
				</td>
			</tr>
			""".format(id=anime.id, title=xmlescape(anime.title), image_url=anime.image_url))
			if not anime.image_url:
				a = animetr.find('td/div/a')
				a.clear()
				a.append(etree.XML('<div>Image<br />not loaded</div>'))

			for obj, data in anime:
				if isinstance(obj, Seiyuu):
					outertd = etree.Element('td', {'class': 'borderClass'}, valign='top', align='right')
					table = etree.Element('table', {'class': 'space_table'}, border='0', cellpadding='0', cellspacing='0')
					for datum in data:
						innertr = etree.Element('tr')
						innertd = etree.XML("""
						<td valign="top" align="right" style="padding: 0 4px" nowrap="">
							{char}
							<br />
							<small>{type}</small>
						</td>
						""".format(char=datum['char'], type=datum['type']))
						innertr.append(innertd)
						table.append(innertr)
					outertd.append(table)
					animetr.append(outertd)

			return animetr

		table = etree.Element('table', border='0', cellpadding='0', cellspacing='0', style='width:auto;')
		table.append(makeheader())
		for a in sorted(animeset, key=lambda a: a.title):
			table.append(makeentry(a))

		return table

	
	def fn_view(self, context, args, data):
		if not args:
			return
		viewtype = 'view_' + args.pop()
		if not hasattr(self, viewtype):
			return
		context.view = getattr(self, viewtype)

	def fn_animelist(self, context, args, data):
		if not args:
			return
		username = args.pop()
		animelist = AnimeList(context, username)
		animelist.populate()
		data.append(AnimeSet(animelist))

	def fn_seiyuu(self, context, args, data):
		if not args:
			return
		id = int(args.pop())
		seiyuu = Seiyuu.make(context, id)
		seiyuu.populate()
		data.append(AnimeSet(seiyuu))

	def fn_intersect(self, context, args, data):
		try:
			right = data.pop()
			left = data.pop()
		except:
			return
		data.append(left.intersect(right))

if __name__ == '__main__':
	from sys import argv
	port = 9495
	if len(argv) > 1:
		port = int(argv[1])
	httpd = HTTPServer(('', port), MalQueryHTTPRequestHandler)
	try:
		httpd.serve_forever()
	except KeyboardInterrupt:
		httpd.server_close()
		sys.exit(0)
