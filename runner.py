#!/usr/bin/env python3

from http.server import HTTPServer, BaseHTTPRequestHandler
import sys
import imp
import webserver

class RunnerHTTPRequestHandler(BaseHTTPRequestHandler):
	server_version = "RunnerHTTP/0.1"

	@classmethod
	def refresh(cls):
		for key in webserver.MalQueryHTTPRequestHandler.__dict__:
			if not (key.startswith('__') or key == 'do_GET'):
				setattr(cls, key, webserver.MalQueryHTTPRequestHandler.__dict__[key])

	def do_GET(self):
		imp.reload(webserver)
		self.refresh()
		return webserver.MalQueryHTTPRequestHandler.do_GET(self)

if __name__ == '__main__':
	httpd = HTTPServer(('', 9495), RunnerHTTPRequestHandler)
	try:
		httpd.serve_forever()
	except KeyboardInterrupt:
		httpd.server_close()
		sys.exit(0)
