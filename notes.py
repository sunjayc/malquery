DONT_RUN_THIS_FILE = 0 / 0

z = list(y.parents)
z.insert(0, y)
for i, p in enumerate(z):
	if i == 0:
		continue
	j = 0
	for t in p.children:
		if t.name == z[i - 1].name:
			t['index'] = j
			j += 1
soup['index'] = 0

# Extracting anime table entries and ids from a seiyuu page
q = soup.select('#content > table > tr > td:nth-of-type(2) > table')[0] # you want the <tr>s from this
trs = q('tr') # it's ok, there aren't any irrelevant <tr>s
urls = [tr.td.div.a['href'] for tr in trs]
import re
ids = [re.search('anime\/(\d+)\/', url).group(1) for url in urls]

"""
a 0
  {'index': 0, 'href': 'http://myanimelist.net/character/24596/Izaya_Orihara'}
td 2
  {'index': 2, 'align': 'right', 'nowrap': None, 'class': ['borderClass'], 'valign': 'top'}
tr 33
  {'index': 33}
table 0
  {'cellpadding': '0', 'cellspacing': '0', 'border': '0', 'index': 0, 'width': '100%'}
td 1
  {'index': 1, 'style': 'padding-left: 5px;', 'valign': 'top'}
tr 0
  {'index': 0}
table 0
  {'cellpadding': '0', 'cellspacing': '0', 'border': '0', 'index': 0, 'width': '100%'}
div 1
  {'index': 1, 'id': 'content'}
div 2
  {'index': 2, 'id': 'contentWrapper'}
div 0
  {'index': 0, 'id': 'myanimelist'}
body 0
  {'onload': '', 'index': 0}
html 0
  {'index': 0}
[document] 0
  {'index': 0}
"""
