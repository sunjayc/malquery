#!/usr/bin/env python3

from maldata import *
from prettyprint import *
from sys import stdin, stdout, argv

def main():
	animelist = AnimeList('shijie.xml')
	animelist.populate()
	anime_ids = [a.id for a in animelist.anime]

	seiyuu = Seiyuu(int(argv[1]))
	seiyuu.populate()

	seiyuu.filter(animelist)

	stdout.write(str(seiyuu.xml))

main()
