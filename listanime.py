#!/usr/bin/env python3

from maldata import *
from prettyprint import *
from operator import attrgetter

@prettyprinter
def main():
	animelist = AnimeList('Shijie')
	animelist.populate()
	entries = sorted([a.title for a in animelist.anime])
	ppmenu(entries, typer=True)

main()
